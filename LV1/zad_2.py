# -*- coding: utf-8 -*-
"""
Created on Tue Nov 03 14:08:16 2015

@author: Sikinchara
"""

ocjena = float(input ("Unesite ocjenu:")) #prebacili ga u broj
if ocjena < 0 or ocjena > 1:
    print ("Greska!")
    exit()
else:
    if ocjena > 0.9 and ocjena < 1:
        print ocjena, "-> Pripada u skupini A"
    elif ocjena > 0.8 and ocjena < 0.9:
        print ocjena, "-> Pripada u skupini B"
    elif ocjena > 0.7 and ocjena < 0.8:
        print ocjena, "-> Pripada u skupini C"
    elif ocjena > 0.6 and ocjena < 0.7:
        print ocjena, "-> Pripada u skupini D"
    else:
        print ocjena, "-> Pripada u skupini F"
    
print "Dovidjenja!"