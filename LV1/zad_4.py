# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 14:35:48 2015

@author: Zvonimir
"""

#4. Zadatak
#-------------------------Unos brojeva u beskonacnu listu---------------------#
print 'Program za unos brojeva. Za prekid upisite: "Done"\n'
uneseni_brojevi = list() #Beskonacna lista u koju ce se unositi brojevi
while (True): #uvjet za beskonacnu petlju 
    unos = raw_input ('Unesite broj: ') 
    if unos in ['Done','done','d0ne','D0ne','don3','Don3']: #ako je unsesena vrijednost 'Done', prekida petlju
        break
    vrijednosti = float(unos) #pridržuje brojcane vrijednosti varijabli 'vrijednosti'
    uneseni_brojevi.append (vrijednosti)#pomocu naredbe .append dodajemo vrijednost na kraj liste
#---------------------------------Matematika----------------------------------# 
kolicina = len(uneseni_brojevi)
sr_vr = sum(uneseni_brojevi)/len(uneseni_brojevi)
minimum = min(uneseni_brojevi)
maksimum = max(uneseni_brojevi)
#-------------------------------------Ispis-----------------------------------# 
print 'Srednja vrijednost: ',sr_vr
print 'Najmanji broj: ',minimum
print 'Najveci broj: ',maksimum
print 'Ukupan broj elemenata: ', kolicina