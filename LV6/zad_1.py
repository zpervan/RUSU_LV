import pybrain as pb
import pandas as pd
import numpy as np
from sklearn import preprocessing
from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import *
from random import normalvariate

#---------------------FUNKCIJA ZA GENERIRANJE PODATAKA-------------------------

def generate_data(n):

     #prva klasa
     n1 = n/2
     x1_1 = np.random.normal(0.0, 2, (n1,1));
     #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
     x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
     y_1 = np.zeros([n1,1])
     temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
     #druga klasa
     n2 = n - n/2
     x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
     y_2 = np.ones([n2,1])
     temp2 = np.concatenate((x_2,y_2),axis = 1)
    
     data = np.concatenate((temp1,temp2),axis = 0)
    
     #permutiraj podatke
     indices = np.random.permutation(n)
     data = data[indices,:]
     return data
     
#------------------------PREDOBRADA PODATAKA-----------------------------------
N = int(input("Unesite N:"))
podaci = generate_data(N)
print("Generirani podaci sa {} podataka".format(N))
print podaci
print("\n")

#Skaliranje i standardizacija podataka
X_skalirano = preprocessing.scale(podaci)
print X_skalirano.min()
print("")
X_skalirano1 = preprocessing.StandardScaler().fit(podaci)
print("")
print X_skalirano1.mean_
print ("")
print X_skalirano1.transform(podaci)

#----------------------DODJELA PODATAKA I UZORAKA------------------------------
#Uzorci, potrebno je podatke podijelit na X1, X2 i Y
podaci_pd = pd.DataFrame(podaci)
podaci_pd.columns = ['x1','x2','y']
##Dodjeljivanje podataka X1, X2 i Y tako da oznacimo odredjeni stupac i dodjelimo
##predvidjenim varijablama
x1_podaci = podaci_pd['x1']
x2_podaci = podaci_pd['x2']
y_podaci = podaci_pd['y']

#podaci_ds = SupervisedDataSet(2,1)


#--------------------------NEURONSKA MREZA-------------------------------------
mreza = buildNetwork(2, 3, 1, bias=True, hiddenclass=TanhLayer)
ucenje = BackpropTrainer(mreza, podaci_pd)

print("Pocetak ucenja")
print(trainer.train())
a = ucenje.trainUntilConvergence(dataset=podaci_pd,
                                  maxEpochs=1000,
                                  verbose=True,
                                  continueEpochs=10,
                                  validationProportion=0.1,
                                  outlayer=softmax)

print("Kraj ucenja")
print(ucenje.train())