import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

#Dodaje valovitu funkciju, funkcija unutar koda
def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

#Dodaje sum, dunkcija unutar koda
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
#Podjeljci po X, sa sto 100 uzoraka po podjeljku
x = np.linspace(1,10,100)
#y-true varijabli dodaje non_func vrijednost
y_true = non_func(x)
#y-measured je y-true plus nadodan sum
y_measured = add_noise(y_true)
#Plotanje y_measured i y_true
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

#Dodavanje random seed-a, nema elemenata koji se ponavljaju (permutacija)
np.random.seed(12)
indeksi = np.random.permutation(len(x))
#indeksiranje train i test vrijednosti
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

#slicing, izdvajanje elemeanata i grupiranja u stupac
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

#indeksiranje elemenata
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
#sklearn.linear_model biblioteka, linearna regresija
linearModel = lm.LinearRegression()
#prolagodba tj. fit uz linearni model
linearModel.fit(xtrain,ytrain)

print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'
#pretpostavlja vrijednosti na temelju linearnih vrijednosti
ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

#Crtanje pravca X kroz predvidjene elemente
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

#2.zadatak_________________________________________________________________________

def zatvorena_forma(xtrain,ytrain):
    #Naredba np.c_ ubacujemo elemente ispred ili iza postojecih elemenata
    duv = np.c_[np.ones(len(xtrain)),xtrain] 
    #dodajemo 1 duzine vektora xtrain u prvi stupac x0 (Dodatna Ulazna Velicina)   
    prvi = np.linalg.inv(np.dot(np.transpose(duv),duv))
    #inverz matrice od umnoska Xt i X
    drugi = np.dot(np.transpose(duv),ytrain)
    #Produkt dva niza/broja
    thetaML = np.dot(prvi,drugi)
    print thetaML

#3.zadatak_________________________________________________________________________

def gradijentni_spust(xtrain,ytrain):
    from sklearn.linear_model import SGDClassifier
    ytrain = np.ravel(ytrain)#za pretvorbu u 1D niz
    alfa = float(input("Unesite alfu: "))
    clf = SGDClassifier(loss="hinge",penalty="l2",alpha=alfa)
    clf.fit(xtrain,ytrain)
    clf.coef_
#potrebno je usporedit sa vrijednostima na liniji koda 56 i 58
    

#4.zadatak__________________________________________________________________________

#Srednja kvadratna pogreska izmedju skupa za testiranje i ucenje
ytrain_p = linearModel.predict(xtrain)
def kv_pogreska(ytest,ytest_p,ytrain,ytrain_p):
    from sklearn.metrics import mean_squared_error
    rez1 = float(mean_squared_error(ytest,ytest_p))
    print ("Kvadratna pogreska na skupu za testiranje iznosi: {:.3f}".format(rez1))
    rez2 = float(mean_squared_error(ytrain,ytrain_p))
    print ("Kvadratna pogreska na skupu za ucenje iznosi: {:.3f}".format(rez2))    
#Koeficijenti determinacije  
def determinacija (ytest,ytest_p,ytrain,ytrain_p):
    from sklearn.metrics import r2_score
    rez3 = float(r2_score(ytest,ytest_p))
    print "Kvadratna pogreska na skupu za testiranje iznosi: {:.3f}".format(rez3)
    rez4 = float(r2_score(ytrain,ytrain_p))
    print "Kvadratna pogreska na skupu za ucenje iznosi: {:.3f}".format(rez4)

#5.zadatak__________________________________________________________________________
def reziduali(xtrain,ytrain):
    ucenje_r = np.mean((linearModel.predict(xtrain) - ytrain) ** 2)
    print("Reziduali nad skupom za ucenje iznosi: {}".format(ucenje_r))
