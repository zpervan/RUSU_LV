//Najava varijabli
float tempC;
float senzor;
float mv;
int tempPin = 0;
int FOPin = 1;
//Temperaturni senzor LM35DZ, spojen na analogni ulaz A0 
//na Arduinu

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  senzor = analogRead(tempPin);
  mv = (senzor/1024)*5000;
  tempC = mv/10;
  float svjetlina = analogRead(FOPin);
  Serial.print(tempC);
  Serial.print(",");
  Serial.print(svjetlina);
  Serial.print("\n");
  delay (1000);
}
