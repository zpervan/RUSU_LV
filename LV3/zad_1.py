import pandas as pd
import numpy as np

#Otvaranje datoteke mtcars.csv pod imenom automobili
automobili = pd.read_csv('mtcars.csv')

#1. 5 automobila sa najvecom potrosnjom

print ("-------------------1----------------------\n")
mpg = automobili.sort('mpg')
print (mpg.set_index('mpg').head())
#indeksirano po mpg, head ispisuje prvih 5 automobila

#2.  3 automobila s najmanjom potrosnjom

print ("-------------------2----------------------\n")
cilindri = automobili.query('cyl == 8').sort('mpg')
#ispise automobile koji sadrze 8 cilindara te ih sortira po potrosnji
print (cilindri.head(3))

#3.  Srednja potrosnja automobila sa 6 cilindara

print("-------------------3----------------------\n")
potrosnja6 = automobili.query('cyl==6').mpg
#Odabire automobile sa 6 cilindara i uzima samo mpg stupac
print("Srednja potrosnja iznosi:{:.2f} mpg\n".format(float(potrosnja6.sum()/len(potrosnja6))))

#4.  Srednja potrosnja automobila s 4 cilindra te mase izmedju 2000

print("-------------------4----------------------\n")
cilindra4 = automobili.query('cyl == 4').query('wt>=2.0 and wt<=2.2')
#Ugnjezdjeno odabiranje autmobila: 4 cilindra i da mu je wt izmedju 2 i 2.2
lbs = cilindra4.query('mpg').mpg
#Filtriranje ostalih podataka od mpg-a
print("Srednja potrosnja:{:.2f}".format(float(lbs.sum()/len(lbs))))

#5.   Manual/automatic (am)
print("-------------------5----------------------\n")
manual = automobili.query("am == False").am
automatic = automobili.query("am == True").am
#True vrijednost za automatic, False za manual
print ("Manual: {0:d}, Automatic: {1:d}".format(len(manual),len(automatic)))

#6.  Automatic i preko 100hp

print("-------------------6----------------------\n")
hp101 = automobili.query("am == True and hp>100").am
#Trazi sve podatke koji imaju am == True i hp>100
print ("Automatic i preko 100 hp: {} ".format(len(hp101)))

#7.   LBS u KG --> 1 lbs = 0.453 kg
print("-------------------7----------------------\n")
automobili.wt*=0.453
print (automobili[['car','wt']])

print("-----------------KRAJ----------------------")
