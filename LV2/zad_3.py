import random
import matplotlib.pyplot as plt
#Najava varijabli
jedan,dva,tri,cetiri,pet,sest=0,0,0,0,0,0

#Najava prazne liste
bacanja = []

#dodjeljivanje random vrijednosti od 1 do 6 u listu
for i in range (100):
    bacanja.append(random.randint(1,6))

#grupiranje vrijednosti 
for i in range(100):
    if bacanja[i] == 1:
        jedan+=1
    elif bacanja[i] == 2:
        dva+=1
    elif bacanja[i] == 3:
        tri+=1
    elif bacanja[i] == 4:
        cetiri+=1
    elif bacanja[i] == 5:
        pet+=1
    else:
        sest+=1
        
#Crtanje histograma
baceno = [jedan,dva,tri,cetiri,pet,sest]
brojevi = [1,2,3,4,5,6]
plt.bar(brojevi,baceno)
plt.xlabel("Izbaceno")
plt.ylabel("Broj")
plt.title("Bacanje kocke 100 puta")
plt.show()

