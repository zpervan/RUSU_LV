import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm

x_train = np.array(200)
x_test = np.array(100)

def generate_data(n):
    
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data

#Zadatak 1--------------------------------------------------------------------
#generiranje podataka
np.random.seed(242)
x_train = generate_data(200)
np.random.seed(12)
x_test = generate_data(100)

#Zadatak 2--------------------------------------------------------------------

#x_train plot - prikaz podataka
plt.title('X_train plot')
fig, ax = plt.subplots()
im = ax.scatter(x_train[:,0], x_train[:,1], c=x_train[:,2], s=80, cmap=plt.cm.jet)
fig.colorbar(im, ax=ax)

#x_test plot - prikaz podataka
plt.title('X_test plot')
fig, ax = plt.subplots()
im = ax.scatter(x_test[:,0], x_test[:,1], c=x_test[:,2], s=80, cmap=plt.cm.jet)
fig.colorbar(im, ax=ax)



#Zadatak 3--------------------------------------------------------------------

logisticModel = lm.LogisticRegression()
logisticModel.fit(x_train[:,0:2],x_train[:,2])
#pomocu logistickog modela dobivamo koeficijente theta 1, 2 i 3
theta0=logisticModel.intercept_
theta1=logisticModel.coef_[0,0]
theta2=logisticModel.coef_[0,1]
#izlucen x2 iz formule kako bi ga izracunali
x1=-8
x2=(-theta0-theta1*x1)/theta2

x11=8
x21=(-theta0-theta1*x1)/theta2

plt.plot(np.array([-8,8]),np.array([x2,x21]))
plt.title("Model dobiven pomocu logisticke regresije")
#Primjeti se da su podaci odijeljeni (granicom odluke), no nije dobra granica
# jer nije podijelio dobro skup za ucenje od skupa za testiranje
#Zadatak 4--------------------------------------------------------------------

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(x_train[:,0])-0.5:max(x_train[:,0])+0.5:.05,
                          min(x_train[:,1])-0.5:max(x_train[:,1])+0.5:.05]

grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = logisticModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)

ax.set_title('Izlaz logisticke regresije')
ax.scatter(x_train[:,0], x_train[:,1],s=80)
plt.show()


#zadatak 5---------------------------------------------------------------------
#provedba klasifikacije testnog skupa
#slicno kao i 3. zadatak samo sa testnim skupom
logisticModel = lm.LogisticRegression()
logisticModel.fit(x_test[:,0:2],x_test[:,2])

theta0t = logisticModel.intercept_
theta1t = logisticModel.coef_[0,0]
theta2t = logisticModel.coef_[0,1] 
# predvidjanje testnog skupa
x_test_predict =  logisticModel.predict(x_test[:,0:2])

razlika = np.subtract(x_test_predict,x_test[:,2])
    
fig, ax = plt.subplots()
#crtanje podataka pomocu scatter plot-a
im = ax.scatter(x_test[:,0], x_test[:,1], c=razlika, s=80)
#gradijent boje
fig.colorbar(im, ax=ax)

#podjela uzoraka 
x1t = -8
x2t = (-theta0t-theta1t*x1t)/theta2t

x11t = 8
x21t = (-theta0t-theta1t*x11t)/theta2t

plt.title("zadatak5")
plt.plot(np.array([-8,8]),np.array([x2t,x21t]))
x_test_model =  logisticModel.predict(x_test[:,0:2])
razlika = np.subtract(x_test_model, x_test[:,2])

#-----------------------------------------------------------------------------

#Zadatak 6--------------------------------------------------------------------

#crtanje matrice zabune
from sklearn.metrics import confusion_matrix 

def plot_confusion_matrix(c_matrix):

 norm_conf = []
 for i in c_matrix:
     a = 0
     tmp_arr = []
     a = sum(i, 0)
     for j in i:
         tmp_arr.append(float(j)/float(a))
     norm_conf.append(tmp_arr)
     
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in xrange(width):
     for y in xrange(height):
        ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                horizontalalignment='center',
                verticalalignment='center', color = 'green', size = 25)
                
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])

 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()
 
#Matrica zabune nad skupom za testiranje---------------------------------------
from sklearn.metrics import confusion_matrix
mz = confusion_matrix(x_test[:,2], x_test_predict)
plot_confusion_matrix(mz)
print ("")
#Accurracy score--------------------------------------------------------------
from sklearn.metrics import accuracy_score
print("Tocnost: {}".format(accuracy_score(x_test[:,2], x_test_predict)))

#Precision score--------------------------------------------------------------
from sklearn.metrics import precision_score
print("Preciznost: {:.2f}".format(precision_score(x_test[:,2], x_test_predict)))

#Recall score------------------------------------------------------------------
from sklearn.metrics import recall_score
print("Odziv: {:.2f}".format(recall_score(x_test[:,2], x_test_predict)))

#Zadatak 7---------------------------------------------------------------------

from sklearn.preprocessing import PolynomialFeatures
poly = PolynomialFeatures(degree=3, include_bias = False)
x_train_new = poly.fit_transform(x_train[:,0:2])

# 3----------------------------------------------------------------------------
logisticModel = lm.LogisticRegression()
logisticModel.fit(x_train_new[:,0:2],x_train[:,2]#dodan x_train_new
#pomocu logistickog modela dobivamo koeficijente theta 1, 2 i 3
theta0=logisticModel.intercept_
theta1=logisticModel.coef_[0,0]
theta2=logisticModel.coef_[0,1]
#izlucen x2 iz formule kako bi ga izracunali
x1=-8
x2=(-theta0-theta1*x1)/theta2

x11=8
x21=(-theta0-theta1*x1)/theta2

plt.plot(np.array([-8,8]),np.array([x2,x21]))
plt.title("Model dobiven pomocu logisticke regresije")

# 4---------------------------------------------------------------------------
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(x_train[:,0])-0.5:max(x_train[:,0])+0.5:.05,
                          min(x_train[:,1])-0.5:max(x_train[:,1])+0.5:.05]

grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = logisticModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)

ax.set_title('Izlaz logisticke regresije')
ax.scatter(x_train_new[:,0], x_train[:,1],s=80) #dodan x_train_new
plt.show()
# 5---------------------------------------------------------------------------
#nova varijabla - x_test_new
x_test_new = poly.fit_transform(x_test[:,0:2]) 
#provedba klasifikacije testnog skupa
#slicno kao i 3. zadatak samo sa testnim skupom
logisticModel = lm.LogisticRegression()
logisticModel.fit(x_test[:,0:2],x_test[:,2])

theta0t = logisticModel.intercept_
theta1t = logisticModel.coef_[0,0]
theta2t = logisticModel.coef_[0,1] 
# predvidjanje testnog skupa
x_test_predict =  logisticModel.predict(x_test_new[:,0:2]) #dodano

razlika = np.subtract(x_test_predict,x_test[:,2])
    
fig, ax = plt.subplots()
#crtanje podataka pomocu scatter plot-a
im = ax.scatter(x_test[:,0], x_test[:,1], c=razlika, s=80)
#gradijent boje
fig.colorbar(im, ax=ax)

#podjela uzoraka 
x1t = -8
x2t = (-theta0t-theta1t*x1t)/theta2t

x11t = 8
x21t = (-theta0t-theta1t*x11t)/theta2t

plt.title("zadatak7.5")
plt.plot(np.array([-8,8]),np.array([x2t,x21t]))
x_test_model =  logisticModel.predict(x_test[:,0:2])
razlika = np.subtract(x_test_model, x_test[:,2])
#6-----------------------------------------------------------------------------

mz = confusion_matrix(x_test_new[:,2], x_test_predict)#dodan x_test_new
plot_confusion_matrix(mz)